#!/bin/sh

MAINT=''
REASON='mia'

usage () {
    echo "Usage: $0 [-r reason] [-b bcc-adress] [-h|--help] maintainer"
    echo ""
    echo "    -h print this help"
    echo "    -b specify the BCC address yourself instead of generate it with mia-*@qa.debian.org"
    echo "    -r give the reason of disclaiming (mia or retired)"
    echo ""
    echo "     maintainer must be in 'Full Name <mail@address>' format."
    echo ""
    exit 1
}

if [ $# -eq 0 ] ; then
  usage
fi

while [ $# -ne 0 ] ; do
  case "$1" in
    -b)
      BCCADD="$2"
      shift
    ;;
    -r)
      if [ "$2" = "mia" -o "$2" = "retired" ] ; then
       REASON="$2"
       shift
      else
       usage
      fi
    ;;
    -h|--help|-*)
       usage
    ;;
    *)
       if [ -z "$MAINT" ] ; then
        MAINT="$1"
       else
        usage
       fi
    ;;
  esac
  shift
done

ADDR="`echo $MAINT | sed -e 's/.*<\(.*\)>.*/\1/'`"
TAG="`echo $MAINT | sed -e 's/.*<\(.*\)>.*/\1/' -e 's/@debian.org$//' -e 's/@/=/'`"

disclaim () {
    BUG=$1
    TITLE="`bts status $BUG fields:subject | sed -e 's/^subject\t//'`"

    case "$TITLE" in
      ITP:*)
        NEWTITLE="`echo $TITLE | sed -e 's/ITP:/RFP:/'`"
      ;;
      ITA:*)
        NEWTITLE="`echo $TITLE | sed -e 's/ITA:/RFP:/'`"
      ;;
      *)
        echo "$BUG -- $TITLE" >> disclaim-failures
        return
      ;;
    esac
  
    case "$REASON" in
      mia) 
        reason_text="is apparently not active anymore, therefore I disclaim this WNNP bug now."
      ;;
      retired) 
        reason_text="has retired, therefore I disclaim this WNNP bug now."
      ;;
    esac

    ( echo ""
      echo "retitle $BUG $NEWTITLE"
      echo "noowner $BUG"
      echo "thanks"
      echo ""
      echo "$MAINT $reason_text"
      echo ""
    ) > disclaim-mail

    if [ ! $BCCADD ] ; then
      BCCADD=mia-$TAG@qa.debian.org
    fi

    if [ $! ] ; then
      exit 1
    else
      mutt -s "Disclaiming WNPP bug $BUG" \
      	-e "my_hdr X-Debbugs-Cc: $MAINT" \
	-e "my_hdr X-MIA-Summary: -\\; disclaiming WNPP bug $BUG" \
	-e "set autoedit" \
	-b "$BCCADD" \
	-i disclaim-mail \
	control@bugs.debian.org
    fi
    rm -f disclaim-mail
}

bug_list="`bts select package:wnpp owner:$ADDR | xargs`"
for bug in $bug_list ; do
    disclaim "$bug"
done

if [ -f disclaim-failures ] ; then
    echo "The following WNPP bugs of $ADDR didn't look like an ITP/ITA."
    echo "You have to check why and deal with them manually:"
    cat disclaim-failures
    rm -f disclaim-failures
fi

# vim:sw=2
