#!/usr/bin/python

# Command-line add an entry to MIA database
# Copyright (C) 2006  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
# Copyright (C) 2007  Luk Claes <luk@debian.org>
# $Id: mia-save-wat 1278 2006-02-12 23:27:46Z jeroen $

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import os, sys, time, re
import apt_pkg, utils, status

os.umask(02)

apt_pkg.init()
Cnf = apt_pkg.Configuration()
apt_pkg.read_config_file_isc(Cnf, status.config)

if len(sys.argv) < 2:
    print >> sys.stderr, "Usage: %s <file> <status>" % (sys.argv[0])
    sys.exit(1)

filename = sys.argv[1]
if not filename:
    print >> sys.stderr, "No file with logins or emails given as parameter"
    sys.exit(1)

newstatus = sys.argv[2]
if not newstatus:
    print >> sys.stderr, "No status given"
    sys.exit(1)

file = open(filename,"r")
for line in file:
    maint = line.strip()
    if not maint:
        print >> sys.stderr, "No maintainer given, pass login or email as parameter"
        sys.exit(1)

    query = "ldap:"+maint
    if maint.find("@") >= 0:
        query = "email:"+maint

    # make sure to not allow shell stuff, /'s, etc!
    if re.compile(r"[^a-z0-9@+._-]").search(maint, re.I):
        print >> sys.stderr, "Invalid characters found in maintainer, aborting"
        sys.exit(1)

    status.write_status(maint.replace("@", "="), time.time(), newstatus, False)
file.close()
# vim: ts=4:expandtab:shiftwidth=4:
