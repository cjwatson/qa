#!/usr/bin/python

# Extract and store section & priority of .deb's
# Copyright (C) 2005  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

################################################################################

import os, string, re, sys;

ftproot = "/srv/qa.debian.org/ftp/debian"

################################################################################

# Unreachable code, for now...
def usage(exit_code=0):
    print """Usage: extract-section-priority <file>
Extract sections and priorities from .deb files, and store them in <file>

  -h, --help                show this help and exit."""
    sys.exit(exit_code)

################################################################################

cache = {}
out = None

def read_cache(file):
    global cache

    f = open(file, 'r')
    for line in f:
        (path, section, priority) = line.split()
        cache[path] = (section, priority)
    f.close()

debline = re.compile("^Filename: (.*)", re.I)

def readall(f):
    for line in f:
        deb_res = debline.search(line)
        if deb_res:
            deb = deb_res.group(1)
            if deb in cache:
                c = cache[deb]
                if c == 1:
                    continue
                else:
                    (section, priority) = c
            else:
                (section, priority) = getdpkginfo(deb)
            out.write("%s %s %s\n" % (deb, section, priority))
            cache[deb] = 1

def getdpkginfo(deb):
    dpkginfo = os.popen("dpkg -I %s/%s control" % (ftproot, deb))
    section = priority = None
    for dpkgline in dpkginfo:
        if dpkgline.find(':') == -1: continue
        (key, value) = dpkgline.split(':', 1)
        key = key.lower()
        if key == "section":
            section = value.strip()
        elif key == "priority":
            priority = value.strip()
    if dpkginfo.close():
        raise Exception, "Failed to read %s" % deb
    return (section, priority)

def main ():
    global out

    target = sys.argv[1]
    try:
        read_cache(target)
    except IOError:
        pass

    out = open("%s.new" % target, 'w')
    # oldoldstable not mentioned as it has no armel
    for suite in [ "oldstable", "stable", "testing", "unstable", "experimental" ]:
        for arch in [ "amd64", "armel", "i386", "mips", "mipsel" ]:
            input = os.popen("/srv/qa.debian.org/data/ftp/get-packages -v debian \
                -s %s -a %s" % (suite, arch), "r")
            readall(input)
            input.close()
    out.close()

    os.rename("%s.new" % target, target)

################################################################################

if __name__ == '__main__':
    main()

# vim: ts=4 sw=4 et
