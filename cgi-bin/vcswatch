#!/usr/bin/perl -T

# Copyright (C) 2014-2018 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use 5.010; # // operator
use warnings;
use strict;
use CGI qw(:standard);
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use Encode qw(encode);
use DBD::Pg;
use Template;
use URI::Escape;

$CGI::POST_MAX = 0;
$CGI::DISABLE_UPLOADS = 1;

my $cgi = new CGI;
my $dbh = DBI->connect("dbi:Pg:service=qa;user=guest", '', '',
	{ AutoCommit => 1, RaiseError => 1, PrintError => 1});

#####################################################################################

my $status = "200 Ok";
my @location = ();
my $title = "vcswatch";
my $body = '';

my $package = $cgi->param('package') // '';
my $p;

if ($package) {
	if ($cgi->param('poke')) {
		$dbh->do("UPDATE vcs SET next_scan = round_time(now()) WHERE package = ? AND next_scan > now()",
			undef, $package);
		$status = "303 See Other";
		my $uri_package = uri_escape($package);
		@location = (-location => "/cgi-bin/vcswatch?package=$uri_package");
	}

	if ($ENV{SSL_CLIENT_S_DN_CN} and $cgi->param('vcsheader') and
		$cgi->param('vcsheader') =~ /^Vcs-(Arch|Bzr|Cvs|Darcs|Git|Hg|Mtn|Svn): (\w[\w.:\/+-]+(?: -b (\w[\w.:\/+-]+))?(?: \[\w\S*\])?)$/) {
		my ($vcs, $url, $branch) = ($1, $2, $3);
		$dbh->do("UPDATE vcs SET vcs = ?, url = ?, branch = COALESCE(?, branch), valid_checkout = false, next_scan = round_time(now()), edited_by = ?, edited_at = now() WHERE package = ?",
			undef, $vcs, $url, $branch, $ENV{SSL_CLIENT_S_DN_CN}, $package);
		$status = "303 See Other";
		@location = (-location => "/cgi-bin/vcswatch?package=$package");
	}

	my $q = $dbh->prepare("SELECT * FROM vcs WHERE package = ?");
	$q->execute($package);
	$p = $q->fetchrow_hashref;
	if ($p) {
		$title = "$package vcswatch";
		if ($p->{status} eq 'COMMITS') {
			my $commits = $p->{commits} > 1 ? "commits" : "commit";
			$p->{status_message} .= "VCS has seen $p->{commits} $commits since the $p->{tag} tag\n";
		} elsif ($p->{status} eq 'NEW') {
			$p->{status_message} .= "VCS has unreleased changes: $p->{changelog_version} > $p->{package_version}\n";
		} elsif ($p->{status} eq 'OLD') {
			$p->{status_message} .= "VCS is behind the version in the archive: $p->{changelog_version} < $p->{package_version}.\n";
			if ($p->{vcs} eq 'Git' and $p->{url} =~ /^http/ and $p->{url} !~ /salsa.debian.org/) {
				$p->{status_message} .= "<p>Hint: This git repository uses http. You might need to run <i>git update-server-info</i> to update the auxiliary info files.</p>\n";
			}
		} elsif ($p->{status} eq 'UNREL') {
			$p->{status_message} .= "VCS matches the version in the archive, but the VCS changelog is $p->{changelog_distribution}. The uploader should update the changelog and tag the release.\n";
		} elsif ($p->{status} eq 'OK') {
			$p->{status_message} .= "VCS matches the version in the archive\n";
		}
		$p->{url_link} = $p->{url};
		$p->{url_link} =~ s/ .*//; # remove -b branch [dir] part
		# encode changelog texts as UTF-8 octets
		for my $f (qw(changelog vcslog)) {
			$p->{$f} = encode('UTF-8', $p->{$f}) if ($p->{$f});
		}

	} else {
		$status = "404 Not Found";
		$body = "<p><b>Error: Unknown package requested</b> (if this is a package in Debian, it doesn't have any Vcs-* headers)</p>";
	}

} else { # no package
	my $tbl = $dbh->selectall_arrayref("select vcs, count(*),
		count(nullif(status='OK', false)) as OK,
		count(nullif(status='COMMITS', false)) as COMMITS,
		count(nullif(status='NEW', false)) as NEW,
		count(nullif(status='OLD', false)) as OLD,
		count(nullif(status='UNREL', false)) as UNREL,
		count(nullif(status='ERROR', false)) as ERROR
		from vcs group by 1 order by 1");
	$body .= "<table border=\"1\">\n";
	$body .= "<tr><th>VCS</th><th>Count</th><th>OK</th><th>COMMITS</th><th>NEW</th><th>OLD</th><th>UNREL</th><th>ERROR</th></tr>\n";
	foreach my $row (@$tbl) {
		$body .= "<tr>";
		foreach my $col (@$row){
			$col //= '';
			$body .= "<td style=\"text-align:right\">$col</td>";
		}
		$body .= "</tr>\n";
	}
	$body .= "</table>\n";
}

print header (-charset => 'utf-8', -status => $status, @location);

my $template = Template->new (INCLUDE_PATH => '/srv/qa.debian.org/web');

$template->process('vcswatch-template.html', {
	AUTHOR => "Christoph Berg",
	TITLE => $title,
	BODY => $body,
	sso => $ENV{SSL_CLIENT_S_DN_CN},
	package => $package,
	p => $p,
}) || die $template->error();

$dbh->disconnect;
